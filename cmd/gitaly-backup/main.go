package main

import "gitlab.com/gitlab-org/gitaly/v16/internal/cli/gitalybackup"

func main() {
	gitalybackup.Main()
}
